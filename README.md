<img src="images/ozcar.jpg"/>

Présentation pour le projet commun du laboratoire EVS-Isthme.
Journée du 07/07/2021

Projet collectif :  Débattre de la « Zone critique »

Le 7 juillet 14h-16h

- 14h : Ouverture de la séance : Michel Depeyre

- 14h15 : Pierre-Olivier MAZAGOL « Bifurcation(s) de la zone critique : l'exemple du barrage de Grangent »
- 14h30 : Georges COLLIN: Les forêts à l’échelle du Pilat du XVIIIe au XXe siècle.
- 14h45 : [Cyrille CONORD : Le crassier : objet « critique »? (Objet situé ou hyperobjet ?)](https://cconord.gitpages.huma-num.fr/zone_critique/)
- 15h : Georges-Henri L	AFFONT : A propos des tonalités affectives de l'urbain anthropocène
- 15h15 : Sarah REAULT : Le Donbass est-il une "zone critique"?
- 15h30 : Geoffrey MOLLE : Des circulations souterraines aux cimes des tours, une approche mésologique de la zone critique en contexte métropolitain.
- 15h45 : Françoise GIRARDOT et Séverine ALLEGRA : La qualité du lac de Devesset, un projet de convergence pour ISTHME?

- 16H00 : Clôture des travaux
